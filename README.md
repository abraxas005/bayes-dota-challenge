bayes-dota
==========

This is the [task](TASK.md).

Dependencies
============

A third party dependency was added for custom Hibernate types used in the domain classes. The dependency is:

```
<dependency>
  <groupId>com.vladmihalcea</groupId>
  <artifactId>hibernate-types-52</artifactId>
  <version>2.10.0</version>
</dependency>
```

Domain
======

For simplicity reasons, the modeling was based on events so only two entities are used:

- Match
- Event

The event entity stores the event specific data as JSON. Since there may be tens of events types, it may be cumbersome
to have the same number of payloads as event types. However, I though this approach was more accessible than using an 
event base entity annotated with `@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)` and several 
entity-per-event-types.

One possible drawback of this approach is that working with this JSON payloads could be required at the database level
which may decrease performance. However, most RDMS's today have functions and operators that allows clients to query 
and manipulate JSON data with not little performance penalty. Another option is to use NoSQL database systems but that
an entire different conversation.

Service
=======

### Inserting match

When ingesting matches, there are two properties that change the behavior of combat log processing:

- `gg.bayes.challenge.enable_save_match_log_file`: Enables saving the combat log file that is associated with a match.
- `gg.bayes.challenge.log_directory`: Specifies the location of where to save the combat log file.

Note that for simplicity, the saving of the combat log files is disabled by default.

### Parsing Events

To parse events, regular expressions were used for time constraint and simplicity reasons. However,
another approach such as tokenize the event line to extract the necessary data could be used.

### Inserting events

Since a combat log file can easily have thousands of events, inserting these records one by one is
extremely inefficient. So batching was enabled in Hibernate with the property 
`spring.jpa.properties.hibernate.jdbc.batch_size` which reduces the number of network trips to the database.

Of course there are lots of other optimizations that can be performed but batching was the only optimization 
implemented due to time constraint reasons.

### Querying events

All of the processing is performed in memory since the H2 database does not have any support for handling JSON data at 
the DB level. However, relevant events are fed up in memory in small batches which will prevents the application to 
unnecessary consume a lot of memory.

To enable this kind of behavior, JPA scrollable results feature is used by making the return of the relevant query as 
`Stream<T>` and setting `org.hibernate.annotations.QueryHints.FETCH_SIZE` to a proper value.

On the other hand, please note that processing all of these events in real time each time the data is requested is 
inefficient. The idea is to have this data cached or aggregated somewhere. However, due to simplicity and time 
constraints reasons, it is done this way.

Testing
=======

Only bare bones testing were added.