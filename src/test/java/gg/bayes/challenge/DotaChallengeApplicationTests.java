package gg.bayes.challenge;

import lombok.SneakyThrows;
import lombok.val;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class DotaChallengeApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    @SneakyThrows
    void postFirstPayloadReturnsOK() {
        val payload = FileUtils.readFileToString(new File("data/combatlog_1.txt"), "UTF-8");

        mockMvc.perform(post("/api/match")
                .contentType(MediaType.TEXT_PLAIN)
                .content(payload))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").value(is(notNullValue())));
    }

    @Test
    @SneakyThrows
    void postSecondPayloadReturnsOK() {
        val payload = FileUtils.readFileToString(new File("data/combatlog_2.txt"), "UTF-8");

        mockMvc.perform(post("/api/match")
                .contentType(MediaType.TEXT_PLAIN)
                .content(payload))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").value(is(notNullValue())));
    }
}
