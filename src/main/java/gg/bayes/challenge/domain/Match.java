package gg.bayes.challenge.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Match extends BaseEntity {

    @Id
    @SequenceGenerator(name = "match_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "match_seq")
    protected Long id;

    @Type(type = "uuid-char")
    @Column(name = "file_id", nullable = false)
    protected UUID fileId;

    @Column(name = "log_path", nullable = false)
    protected String logPath;

    //add extra columns like game version, created by, created on, etc
}
