package gg.bayes.challenge.domain;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;

@Data
@MappedSuperclass
@TypeDef(name = "json", typeClass = JsonStringType.class)
public abstract class BaseEntity implements Serializable {

    @Version
    @Setter(AccessLevel.NONE)
    protected Long version;
}
