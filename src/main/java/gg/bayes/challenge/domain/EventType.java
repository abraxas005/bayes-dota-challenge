package gg.bayes.challenge.domain;

public enum EventType {

    HERO_PURCHASE_ITEM,
    HERO_KILLS_HERO,
    HERO_CAST_SPELL,
    HERO_DAMAGE_TO_HERO;
}
