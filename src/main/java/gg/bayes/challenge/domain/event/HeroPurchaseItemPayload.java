package gg.bayes.challenge.domain.event;

import gg.bayes.challenge.domain.EventType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class HeroPurchaseItemPayload extends EventPayload {

    public HeroPurchaseItemPayload() {
        super(EventType.HERO_PURCHASE_ITEM);
    }

    protected String hero;
    protected String item;
}
