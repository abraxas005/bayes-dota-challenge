package gg.bayes.challenge.domain.event;

import gg.bayes.challenge.domain.EventType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class HeroKillsHeroPayload extends EventPayload {

    public HeroKillsHeroPayload() {
        super(EventType.HERO_KILLS_HERO);
    }

    protected String hero;
    protected String target;
}
