package gg.bayes.challenge.domain.event;

public interface EventPayloadTypes {

    String HERO_PURCHASE_ITEM = "HERO_PURCHASE_ITEM",
            HERO_KILLS_HERO = "HERO_KILLS_HERO",
            HERO_CAST_SPELL = "HERO_CAST_SPELL",
            HERO_DAMAGE_TO_HERO = "HERO_DAMAGE_TO_HERO";
}
