package gg.bayes.challenge.domain.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import gg.bayes.challenge.domain.EventType;
import lombok.*;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class HeroCastSpellPayload extends EventPayload {

    public HeroCastSpellPayload() {
        super(EventType.HERO_CAST_SPELL);
    }

    protected String hero;
    @JsonProperty("spell_name")
    protected String spellName;
    @JsonProperty("spell_level")
    protected int spellLevel;
    protected String target;
}
