package gg.bayes.challenge.domain.event;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import gg.bayes.challenge.domain.EventType;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.EXISTING_PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;
import static gg.bayes.challenge.domain.event.EventPayloadTypes.*;

@Data
@JsonSubTypes(value = {
        @Type(value = HeroPurchaseItemPayload.class, name = HERO_PURCHASE_ITEM),
        @Type(value = HeroKillsHeroPayload.class, name = HERO_KILLS_HERO),
        @Type(value = HeroCastSpellPayload.class, name = HERO_CAST_SPELL),
        @Type(value = HeroDamageToHeroPayload.class, name = HERO_DAMAGE_TO_HERO)
})
@JsonTypeInfo(use = NAME, include = EXISTING_PROPERTY, property = EventPayload.PAYLOAD_TYPE_PROPERTY)
public abstract class EventPayload implements Serializable {

    public static final String PAYLOAD_TYPE_PROPERTY = "payloadType";

    @NonNull
    protected final EventType payloadType;

    public <T extends EventPayload> T unwrap() {
        return (T) this;
    }
}
