package gg.bayes.challenge.domain.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import gg.bayes.challenge.domain.EventType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class HeroDamageToHeroPayload extends EventPayload {

    public HeroDamageToHeroPayload() {
        super(EventType.HERO_DAMAGE_TO_HERO);
    }

    protected String hero;
    protected String target;
    @JsonProperty("hit_object")
    protected String hitObject;
    protected long damage;
    @JsonProperty("start_health")
    protected Long startHealth;
    @JsonProperty("end_health")
    protected Long endHealth;
}
