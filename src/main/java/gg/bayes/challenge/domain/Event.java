package gg.bayes.challenge.domain;

import gg.bayes.challenge.domain.event.EventPayload;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.Duration;

@Data
@Entity
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Event extends BaseEntity {

    @Id
    @SequenceGenerator(name = "event_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_seq")
    protected Long id;

    @Column(name = "match_id", insertable = false, updatable = false)
    protected Long matchId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "match_id", nullable = false, updatable = false)
    protected Match match;

    @Setter(AccessLevel.NONE)
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    protected EventType type;

    @Column(name = "line_number", nullable = false)
    protected Long lineNumber;

    @Type(type = "Duration")
    @Column(nullable = false)
    protected Duration timestamp;

    @Type(type = "json")
    @Column(nullable = false)
    protected EventPayload data;

    public void setMatch(Match match) {
        if (null != match) {
            matchId = match.getId();
        }
        this.match = match;
    }

    public void setData(EventPayload data) {
        if (null != data) {
            type = data.getPayloadType();
        }
        this.data = data;
    }
}
