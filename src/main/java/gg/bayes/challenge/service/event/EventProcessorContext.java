package gg.bayes.challenge.service.event;

import gg.bayes.challenge.domain.Event;
import gg.bayes.challenge.domain.Match;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
public class EventProcessorContext {

    @NonNull
    Match match;
    @NonNull
    List<Event> events;
}
