package gg.bayes.challenge.service.event.processor;

import gg.bayes.challenge.domain.Event;
import gg.bayes.challenge.domain.event.HeroDamageToHeroPayload;
import gg.bayes.challenge.service.event.EventLine;
import gg.bayes.challenge.service.event.EventProcessorContext;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HeroDamageToHeroEventProcessor extends AbstractEventProcessor {

    protected static final Pattern PATTERN = Pattern.compile("^\\[(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})] npc_dota_hero_(\\w+) hits npc_dota_hero_(\\w+) with (\\w+) for (\\d+) damage( \\((\\d+)->(\\d+)\\))?$");
    protected static final String ITEM_PREFIX = "item_", EMPTY_STRING = "";

    @Override
    public boolean supports(@NonNull EventLine eventLine) {
        return PATTERN.matcher(eventLine.getLine()).matches();
    }

    @Override
    protected void fillEvent(EventLine eventLine, EventProcessorContext context, Event event) {
        final Matcher matcher = PATTERN.matcher(eventLine.getLine());
        matcher.find();

        event.setTimestamp(parseDuration(matcher.group(1)));

        final HeroDamageToHeroPayload payload = new HeroDamageToHeroPayload();
        payload.setHero(matcher.group(2));
        payload.setTarget(matcher.group(3));
        payload.setHitObject(matcher.group(4).replaceFirst(ITEM_PREFIX, EMPTY_STRING));
        payload.setDamage(Long.parseLong(matcher.group(5)));
        //group 6 is not used because it contains the whole "\s(111->222)" section of the string
        Optional.ofNullable(matcher.group(7))
                .map(Long::valueOf)
                .ifPresent(payload::setStartHealth);
        Optional.ofNullable(matcher.group(8))
                .map(Long::valueOf)
                .ifPresent(payload::setEndHealth);

        event.setData(payload);
    }
}
