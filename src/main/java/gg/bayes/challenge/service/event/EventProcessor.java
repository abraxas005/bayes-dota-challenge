package gg.bayes.challenge.service.event;

import lombok.NonNull;

public interface EventProcessor {

    boolean supports(@NonNull EventLine eventLine);

    void process(@NonNull EventLine eventLine, @NonNull EventProcessorContext context);
}
