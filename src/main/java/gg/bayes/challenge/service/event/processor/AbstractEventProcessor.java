package gg.bayes.challenge.service.event.processor;

import gg.bayes.challenge.domain.Event;
import gg.bayes.challenge.repository.EventRepository;
import gg.bayes.challenge.service.event.EventLine;
import gg.bayes.challenge.service.event.EventProcessor;
import gg.bayes.challenge.service.event.EventProcessorContext;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.time.LocalTime;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractEventProcessor implements EventProcessor {

    @Autowired
    protected EventRepository eventRepository;

    @Override
    public void process(@NonNull EventLine eventLine, @NonNull EventProcessorContext context) {
        if (!supports(eventLine)) {
            throw new IllegalStateException(String.format("Called %s with unsupported event", this.getClass().getSimpleName()));
        }

        final Event event = new Event();
        event.setMatch(context.getMatch());
        event.setLineNumber(eventLine.getIndex());

        fillEvent(eventLine, context, event);
        context.getEvents().add(event);

        log.debug("Added event [{}] to processor context", event);
    }

    protected abstract void fillEvent(EventLine eventLine, EventProcessorContext context, Event event);

    protected Duration parseDuration(String timestamp) {
        return Duration.between(LocalTime.MIN, LocalTime.parse(timestamp));
    }
}
