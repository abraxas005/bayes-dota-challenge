package gg.bayes.challenge.service.event.processor;

import gg.bayes.challenge.domain.Event;
import gg.bayes.challenge.domain.event.HeroKillsHeroPayload;
import gg.bayes.challenge.service.event.EventLine;
import gg.bayes.challenge.service.event.EventProcessorContext;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HeroKillsHeroEventProcessor extends AbstractEventProcessor {

    protected static final Pattern PATTERN = Pattern.compile("^\\[(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})] npc_dota_hero_(\\w+) is killed by npc_dota_hero_(\\w+)$");

    @Override
    public boolean supports(@NonNull EventLine eventLine) {
        return PATTERN.matcher(eventLine.getLine()).matches();
    }

    @Override
    protected void fillEvent(EventLine eventLine, EventProcessorContext context, Event event) {
        final Matcher matcher = PATTERN.matcher(eventLine.getLine());
        matcher.find();

        event.setTimestamp(parseDuration(matcher.group(1)));

        final HeroKillsHeroPayload payload = new HeroKillsHeroPayload();
        payload.setTarget(matcher.group(2));
        payload.setHero(matcher.group(3));

        event.setData(payload);
    }
}
