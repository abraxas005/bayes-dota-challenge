package gg.bayes.challenge.service.event;

import lombok.NonNull;
import lombok.Value;

@Value
public class EventLine {
    long index;
    @NonNull
    String line;
}
