package gg.bayes.challenge.service.impl;

import gg.bayes.challenge.domain.Event;
import gg.bayes.challenge.domain.EventType;
import gg.bayes.challenge.domain.Match;
import gg.bayes.challenge.domain.event.HeroCastSpellPayload;
import gg.bayes.challenge.domain.event.HeroDamageToHeroPayload;
import gg.bayes.challenge.domain.event.HeroKillsHeroPayload;
import gg.bayes.challenge.domain.event.HeroPurchaseItemPayload;
import gg.bayes.challenge.repository.EventRepository;
import gg.bayes.challenge.repository.MatchRepository;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;
import gg.bayes.challenge.service.MatchService;
import gg.bayes.challenge.service.event.EventLine;
import gg.bayes.challenge.service.event.EventProcessor;
import gg.bayes.challenge.service.event.EventProcessorContext;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class MatchServiceImpl implements MatchService {

    protected final MatchRepository matchRepository;
    protected final EventRepository eventRepository;
    protected final List<? extends EventProcessor> eventProcessors;
    protected final EntityManager entityManager;

    @Value("${gg.bayes.challenge.enable_save_match_log_file}")
    private boolean enableSaveMatchLogFile;

    @Value("${gg.bayes.challenge.log_directory}")
    private String matchLogDirectory;

    @Override
    @Transactional
    public Long ingestMatch(@NonNull String payload) {
        final Match match = new Match();
        match.setFileId(UUID.randomUUID());
        match.setLogPath(writeCombatLog(match.getFileId(), payload));

        final List<Pair<? extends EventProcessor, EventProcessorContext>> processorsWithContexts = eventProcessors.parallelStream()
                .map(eventProcessor -> Pair.of(eventProcessor, new EventProcessorContext(match, Collections.synchronizedList(new ArrayList<>()))))
                .collect(Collectors.toList());

        final String[] lines = payload.split(System.lineSeparator());
        IntStream.range(0, lines.length)
                .parallel()
                .mapToObj(index -> new EventLine(index, lines[index]))
                .forEach(eventLine -> processorsWithContexts.parallelStream()
                        .filter(pair -> pair.getLeft().supports(eventLine))
                        .findAny()
                        .ifPresent(pair -> pair.getLeft().process(eventLine, pair.getRight())));

        final List<Event> allEvents = processorsWithContexts.parallelStream()
                .map(Pair::getRight)
                .map(EventProcessorContext::getEvents)
                .flatMap(Collection::parallelStream)
                //TODO: Remove the sort
                .sorted(Comparator.comparing(Event::getLineNumber))
                .collect(Collectors.toList());

        matchRepository.save(match);
        eventRepository.saveAll(allEvents);

        log.info("Extracted [{}] events from a total of [{}] lines for match with id [{}]", allEvents.size(), lines.length, match.getId());

        return match.getId();
    }

    @Override
    @Transactional(readOnly = true)
    public List<HeroKills> getMatch(@NonNull Long matchId) {
        try (final Stream<Event> events = eventRepository.findAllByMatchIdAndType(matchId, EventType.HERO_KILLS_HERO)) {
            return events.parallel()
                    .peek(entityManager::detach)
                    .map(Event::getData)
                    .map(HeroKillsHeroPayload.class::cast)
                    .collect(Collectors.groupingBy(HeroKillsHeroPayload::getHero, Collectors.counting()))
                    .entrySet()
                    .parallelStream()
                    .map(entry -> new HeroKills(entry.getKey(), entry.getValue().intValue()))
                    .sorted(Comparator.comparing(HeroKills::getKills).reversed().thenComparing(HeroKills::getHero))
                    .collect(Collectors.toList());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<HeroItems> getItems(@NonNull Long matchId, @NonNull String heroName) {
        try (final Stream<Event> events = eventRepository.findAllByMatchIdAndType(matchId, EventType.HERO_PURCHASE_ITEM)) {
            return events.parallel()
                    .peek(entityManager::detach)
                    .filter(event -> event.getData().<HeroPurchaseItemPayload>unwrap().getHero().equalsIgnoreCase(heroName))
                    .map(event -> {
                        final HeroPurchaseItemPayload payload = event.getData().unwrap();
                        return new HeroItems(payload.getItem(), event.getTimestamp().toMillis());
                    })
                    .sorted(Comparator.comparing(HeroItems::getTimestamp))
                    .collect(Collectors.toList());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<HeroSpells> getSpells(@NonNull Long matchId, @NonNull String heroName) {
        try (final Stream<Event> events = eventRepository.findAllByMatchIdAndType(matchId, EventType.HERO_CAST_SPELL)) {
            return events.parallel()
                    .peek(entityManager::detach)
                    .map(Event::getData)
                    .map(HeroCastSpellPayload.class::cast)
                    .filter(payload -> payload.getHero().equalsIgnoreCase(heroName))
                    .collect(Collectors.groupingBy(HeroCastSpellPayload::getSpellName, Collectors.counting()))
                    .entrySet()
                    .parallelStream()
                    .map(entry -> new HeroSpells(entry.getKey(), entry.getValue().intValue()))
                    .sorted(Comparator.comparing(HeroSpells::getCasts).reversed().thenComparing(HeroSpells::getSpell))
                    .collect(Collectors.toList());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<HeroDamage> getDamage(@NonNull Long matchId, @NonNull String heroName) {
        try (final Stream<Event> events = eventRepository.findAllByMatchIdAndType(matchId, EventType.HERO_DAMAGE_TO_HERO)) {
            return events.parallel()
                    .peek(entityManager::detach)
                    .map(Event::getData)
                    .map(HeroDamageToHeroPayload.class::cast)
                    .filter(payload -> payload.getHero().equalsIgnoreCase(heroName))
                    .collect(Collectors.groupingBy(HeroDamageToHeroPayload::getTarget, Collectors.summarizingLong(HeroDamageToHeroPayload::getDamage)))
                    .entrySet()
                    .parallelStream()
                    .map(entry -> new HeroDamage(entry.getKey(), Long.valueOf(entry.getValue().getCount()).intValue(), Long.valueOf(entry.getValue().getSum()).intValue()))
                    .sorted(Comparator.comparing(HeroDamage::getTotalDamage).reversed().thenComparing(HeroDamage::getDamageInstances).thenComparing(HeroDamage::getTarget))
                    .collect(Collectors.toList());
        }
    }

    @SneakyThrows
    protected String writeCombatLog(UUID fileName, String payload) {
        final File file = new File(matchLogDirectory, fileName + ".txt");

        if (enableSaveMatchLogFile) {
            try (BufferedWriter bw = Files.newBufferedWriter(file.toPath(), StandardOpenOption.CREATE_NEW)) {
                bw.write(payload);
            }
        }

        return file.getPath();
    }
}
